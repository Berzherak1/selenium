package Lesson4.HomeWork;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;

import javax.lang.model.util.Elements;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/*Задание 7. Сделайте сценарий, проходящий по всем разделам админки
 * Сделайте сценарий, который выполняет следующие действия в учебном приложении litecart.
 * 1) входит в панель администратора http://localhost/litecart/admin
 * 2) прокликивает последовательно все пункты меню слева, включая вложенные пункты
 * 3) для каждой страницы проверяет наличие заголовка (то есть элемента с тегом h1)
 */

public class ClickMenuItem {

    private WebDriver driver;

    @Before
    public void startChrome() {

        ChromeOptions options = new ChromeOptions();
        options.setBinary("C:\\Users\\Serg\\AppData\\Local\\Google\\Chrome\\Application\\chrome.exe");
        options.addArguments("user-data-dir=C:\\Users\\Serg\\AppData\\Local\\Google\\Chrome\\User Data\\Default1");
        options.addArguments("--incognito");
        options.addArguments("--start-maximized");
        driver = new ChromeDriver(options);
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
    }

    @Test
    public void clickMenuItem() {
        driver.get("http://localhost/litecart/admin");
        driver.findElement(By.name("username")).sendKeys("admin");
        driver.findElement(By.name("password")).sendKeys("admin");
        driver.findElement(By.name("login")).click();

        List<WebElement> elementsOfMenu = new ArrayList<WebElement>();
        List<WebElement> elementsOfMenuItem = new ArrayList<WebElement>();

        elementsOfMenu = driver.findElements(By.xpath("//ul[@id='box-apps-menu' and @class='list-vertical']/li"));
        int i = 1;

        for (WebElement elementMenu : elementsOfMenu) {

            By menu = By.xpath("//ul[@id='box-apps-menu' and @class='list-vertical']//li[@id='app-'][" + i + "]");
            driver.findElement(menu).click();
            if (areLocatorsPresent(By.xpath("//ul[@class='docs']"))) {

                elementsOfMenuItem = driver.findElements(By.xpath("//ul[@class='docs']/li"));
                int j = 1;
                for (WebElement elementMenuItem : elementsOfMenuItem) {

                    By menuItem = By.xpath("(//ul[@class='docs']/li)[" + j + "]");
                    driver.findElement(menuItem).click();

                    try {
                        checkHead();
                    } catch (AssertionError e) {
                        e.printStackTrace();
                    }
                    j++;
                }

            } else {
                try {
                    checkHead();
                } catch (AssertionError e) {
                    e.printStackTrace();
                }
            }
            i++;
        }
    }

    public boolean areLocatorsPresent(By locator) {
        try {
            return driver.findElements(locator).size() > 0;
        } catch (org.openqa.selenium.NoSuchElementException e) {
            return false;
        }
    }

    public void checkHead() {
        Assert.assertTrue("Нет заголовка по данному локатору", areLocatorsPresent(By.xpath("//h1//span[@class='fa-stack icon-wrapper']")));
    }

    @After
    public void closeChrome() {
        driver.quit();
        driver = null;
    }
}
