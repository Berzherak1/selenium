package Lesson2;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import static org.openqa.selenium.support.ui.ExpectedConditions.titleIs;

/*
Задание 1. Подготовьте инфраструктуру
Подготовьте инфраструктуру для выполнения домашних заданий:
1) Создайте репозиторий на сервисе GitHub, в который будут отправляться домашние задания. Для тех, кто раньше не работал с этим сервисом, внизу есть видео-инструкция. (любители C# при желании могут использовать сервис visualstudio.com, в этом случае не забудьте предоставить доступ тренерам к репозиторию -- barancev@gmail.com и cherednichenko@outlook.com).
2) Установите всё необходимое для работы программное обеспечение.
3) Создайте проект внутри локальной копии репозитория, подключите к проекту Selenium и сделайте небольшой работающий тест, который просто запускает браузер, открывает какую-нибудь страницу и закрывает браузер.
4) Уложите всё в репозиторий и синхронизируйте с удалённым сервером.
В качестве ответа на задание отправьте ссылку на свой репозиторий.
*/

public class MyFirstTest {
    private WebDriver driver;
    private WebDriverWait wait;

    @Before
    public void start(){
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver,10);
    }

    @Test
    public void myFirstTest(){
        driver.get("https://yandex.ru/");
        driver.findElement(By.name("text")).sendKeys("webdriver");
        driver.findElement(By.className("search2__button")).click();
        wait.until(titleIs("webdriver — Яндекс: нашлось 6 млн результатов"));
    }

    @After
    public void stop(){
        driver.quit();
        driver = null;
    }
}
