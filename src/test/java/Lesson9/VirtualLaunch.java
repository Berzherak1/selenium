package Lesson9;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Platform;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.MalformedURLException;
import java.net.URL;

public class VirtualLaunch {

    private RemoteWebDriver driver;
    private WebDriverWait wait;

    @Before
    public void startChrome() throws MalformedURLException {

        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setBrowserName("chrome");
        capabilities.setPlatform(Platform.WINDOWS);
        driver = new RemoteWebDriver(new URL("http://192.168.1.40:4444/wd/hub"), capabilities);
    }

    @Test
    public void test() {
        driver.get("http://yandex.ru/");
    }

    @After
    public void closeBrowser() {
        driver.quit();
        driver = null;
    }
}


