package Lesson5;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.util.concurrent.TimeUnit;

public class LaunchIE {

    private WebDriver driver;

   // @Before
    public void startIE() {
        DesiredCapabilities ieCapabilities = DesiredCapabilities.internetExplorer();

        ieCapabilities.setCapability("nativeEvents", false);
        ieCapabilities.setCapability("unexpectedAlertBehaviour", "accept");
        ieCapabilities.setCapability("ignoreProtectedModeSettings", true);
        ieCapabilities.setCapability("disable-popup-blocking", true);
        ieCapabilities.setCapability("enablePersistentHover", true);
        InternetExplorerOptions options = new InternetExplorerOptions(ieCapabilities);
        //options.setCapability(InternetExplorerDriver.REQUIRE_WINDOW_FOCUS, true);
        driver = new InternetExplorerDriver(options);
        //driver.manage().window().maximize();
    }


    @Before
    public void startChrome() {
        ChromeOptions options = new ChromeOptions();
        options.setBinary("C:\\Users\\Serg\\AppData\\Local\\Google\\Chrome\\Application\\chrome.exe");
        options.addArguments("user-data-dir=C:\\Users\\Serg\\AppData\\Local\\Google\\Chrome\\User Data\\Default1");
        options.addArguments("--incognito");
        options.addArguments("--start-maximized");
        driver = new ChromeDriver(options);
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
    }

    @Test
    public void clickButtonFind() {

        driver.get("http://yandex.ru");
        driver.findElement(By.xpath("//button[contains(@class,'button suggest2-form__button')]")).click();

//        driver.get("http://a24.biz/reg");
//        //driver.findElement(By.xpath("//span[contains(@class,'form__policy__label__checkbox')]")).click();
//       if(!(driver.findElement(By.xpath("//span[contains(@class,'form__policy__label__checkbox')]")).isSelected())){
//           System.out.println("fefef");
//       }
    }

}