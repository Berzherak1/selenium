package Lesson6.HomeWork;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.nio.file.Paths;
import java.util.concurrent.TimeUnit;

import static java.lang.Thread.sleep;
import static org.openqa.selenium.support.ui.ExpectedConditions.titleIs;

/*Сделайте сценарий для добавления нового товара (продукта) в учебном приложении litecart (в админке).
* Для добавления товара нужно открыть меню Catalog, в правом верхнем углу нажать кнопку "Add New Product",
* заполнить поля с информацией о товаре и сохранить.
* Достаточно заполнить только информацию на вкладках General, Information и Prices.
* Скидки (Campains) на вкладке Prices можно не добавлять.
*Переключение между вкладками происходит не мгновенно, поэтому после переключения можно сделать небольшую паузу
*(о том, как делать более правильные ожидания, будет рассказано в следующих занятиях).
* Картинку с изображением товара нужно уложить в репозиторий вместе с кодом. При этом указывать в коде полный абсолютный путь к файлу плохо, на другой машине работать не будет. Надо средствами языка программирования преобразовать относительный путь в абсолютный.*/

public class AddProduct {
    private WebDriver driver;
    private WebDriverWait wait;

    @Before
    public void startChrome() {

        ChromeOptions options = new ChromeOptions();
        options.setBinary("C:\\Users\\Serg\\AppData\\Local\\Google\\Chrome\\Application\\chrome.exe");
        options.addArguments("user-data-dir=C:\\Users\\Serg\\AppData\\Local\\Google\\Chrome\\User Data\\Default1");
        options.addArguments("--incognito");
        options.addArguments("--start-maximized");
        driver = new ChromeDriver(options);
        wait = new WebDriverWait(driver, 10);
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
    }

    @Test
    public void testAddProduct() throws InterruptedException {
        driver.get("http://localhost/litecart/admin/logout.php");
        driver.findElement(By.name("username")).sendKeys("admin");
        driver.findElement(By.name("password")).sendKeys("admin");
        driver.findElement(By.name("remember_me")).click();
        driver.findElement(By.name("login")).click();
        wait.until(titleIs("My Store"));

        String nameOfNewProduct = "testProduct";
        File file = new File("Без названия.jpg");
        String absolutePath = file.getAbsolutePath();
        driver.findElement(By.xpath("//ul[@id= 'box-apps-menu']//span[contains(text(),'Catalog')]")).click();
        driver.findElement(By.xpath("//a[@class='button' and " +
                "@href='http://localhost/litecart/admin/?category_id=0&app=catalog&doc=edit_product']")).click();
        driver.findElement(By.xpath("//input[@type='radio'][@value='1']")).click();
        driver.findElement(By.xpath("//input[@type='text'][@name='name[en]']")).sendKeys(nameOfNewProduct);
        driver.findElement(By.xpath("//input[@type='text'][@name='code']")).sendKeys("12345");
        driver.findElement(By.xpath("//input[@type='checkbox'][@value='1-2']")).click();
        driver.findElement(By.xpath("//input[@type='number'][@name='quantity']")).sendKeys("100");
        driver.findElement(By.xpath("//input[@type='file'][@name='new_images[]']")).sendKeys(absolutePath);
        driver.findElement(By.xpath("//input[@type='date'][@name='date_valid_from']")).sendKeys("11111111");
        driver.findElement(By.xpath("//input[@type='date'][@name='date_valid_to']")).sendKeys("22122222");
        driver.findElement(By.xpath("//a[@href='#tab-information']")).click();
        sleep(3000);
        driver.findElement(By.xpath("//select[@name='manufacturer_id']/option[@value='1']")).click();
        driver.findElement(By.xpath("//input[@type='text'][@name='keywords']")).sendKeys("keywords");
        driver.findElement(By.xpath("//input[@type='text'][@name='short_description[en]']")).sendKeys("test");
        driver.findElement(By.xpath("//div[@class='trumbowyg-editor']")).sendKeys("description - test");
        driver.findElement(By.xpath("//input[@type='text'][@name='head_title[en]']")).sendKeys("test");
        driver.findElement(By.xpath("//input[@type='text'][@name='meta_description[en]']")).sendKeys("test");
        driver.findElement(By.xpath("//a[@href='#tab-prices']")).click();
        sleep(3000);
        driver.findElement(By.xpath("//input[@type='number'][@name='purchase_price']")).sendKeys("111,11");
        driver.findElement(By.xpath("//select[@name='purchase_price_currency_code']//option[@value='USD']")).click();
        driver.findElement(By.xpath("//input[@type='text'][@name='prices[USD]']")).sendKeys("11");
        driver.findElement(By.xpath("//input[@type='text'][@name='prices[EUR]']")).sendKeys("11");
        driver.findElement(By.xpath("//button[@type='submit'][@name='save']")).click();
        driver.get("http://localhost/litecart/admin/?app=catalog&doc=catalog");
        String nameOfMyProductInTable = driver.findElement(By.xpath("//table[@class='dataTable']//a[contains(text()," +
                "'" + nameOfNewProduct + "')]")).getText();
        Assert.assertTrue("Нет в таблице нового продукта", nameOfNewProduct.equals(nameOfMyProductInTable));
    }

    public void closeBrowser(){
        driver.quit();
        driver = null;
    }
}
