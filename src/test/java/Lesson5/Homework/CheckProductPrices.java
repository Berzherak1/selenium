package Lesson5.Homework;

import com.sun.javafx.scene.control.skin.ColorPalette;
import org.apache.http.util.Asserts;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.Color;

import java.util.*;
import java.util.concurrent.TimeUnit;

/*
* Сделайте сценарий, который проверяет, что при клике на товар открывается правильная страница товара в учебном приложении litecart.
* Более точно, нужно открыть главную страницу, выбрать первый товар в блоке Campaigns и проверить следующее:
* а) на главной странице и на странице товара совпадает текст названия товара
* б) на главной странице и на странице товара совпадают цены (обычная и акционная)
* в) обычная цена зачёркнутая и серая (можно считать, что "серый" цвет это такой, у которого в RGBa представлении одинаковые значения для каналов R, G и B)
* г) акционная жирная и красная (можно считать, что "красный" цвет это такой, у которого в RGBa представлении каналы G и B имеют нулевые значения)
* (цвета надо проверить на каждой странице независимо, при этом цвета на разных страницах могут не совпадать)
* д) акционная цена крупнее, чем обычная (это тоже надо проверить на каждой странице независимо)
* Необходимо убедиться, что тесты работают в разных браузерах, желательно проверить во всех трёх ключевых браузерах (Chrome, Firefox, IE).
*/

public class CheckProductPrices {

    private WebDriver driver;

    @Test
    public void startFirefox() {
        FirefoxOptions options = new FirefoxOptions();
        System.setProperty("webdriver.gecko.driver", "C:\\Webserver\\geckodriver.exe");
        options.addArguments("--incognito");
        options.addArguments("---start-maximized");
        FirefoxProfile firefoxProfile = new FirefoxProfile();
        firefoxProfile.setPreference("browser.private.browsing.autostart", true);
        options.setProfile(firefoxProfile);
        options.addPreference("browser.cache.disk.enable", false);
        driver = new FirefoxDriver(options);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        checkSort();
    }

    @Test
    public void startIE() {
        DesiredCapabilities ieCapabilities = DesiredCapabilities.internetExplorer();
        ieCapabilities.setCapability("nativeEvents", false);
        ieCapabilities.setCapability("unexpectedAlertBehaviour", "accept");
        ieCapabilities.setCapability("ignoreProtectedModeSettings", true);
        ieCapabilities.setCapability("disable-popup-blocking", true);
        ieCapabilities.setCapability("enablePersistentHover", true);
        InternetExplorerOptions options = new InternetExplorerOptions(ieCapabilities);
        options.setCapability(InternetExplorerDriver.REQUIRE_WINDOW_FOCUS, true);
        driver = new InternetExplorerDriver(options);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        checkSort();
    }

    @Test
    public void startChrome() {
        ChromeOptions options = new ChromeOptions();
        options.setBinary("C:\\Users\\Serg\\AppData\\Local\\Google\\Chrome\\Application\\chrome.exe");
        options.addArguments("user-data-dir=C:\\Users\\Serg\\AppData\\Local\\Google\\Chrome\\User Data\\Default1");
        options.addArguments("--incognito");
        options.addArguments("--start-maximized");
        driver = new ChromeDriver(options);
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        checkSort();
    }

    public void checkSort() {
        driver.get("http://localhost/litecart/en/");
        By nameOfProductInBlockCampaigns = By.xpath("//div[@id='box-campaigns']//li[contains(@class,'product')]" +
                "//div[@class='name']");
        By nameOfProductOnPageOfProduct = By.xpath("//h1[@itemprop='name']");
        By regularPriceInMainPage = By.xpath("//div[@id='box-campaigns']//li[contains(@class,'product')]" +
                "//s[@class='regular-price']");
        By regularPriceOnPageOfProduct = By.xpath("//div[@class='information']//s[@class='regular-price']");
        By campaignPriceInCampaigns = By.xpath("//div[@id='box-campaigns']//li[contains(@class,'product')]" +
                "//strong[@class='campaign-price']");
        By campaignPriceOnPageOfProduct = By.xpath("//div[@class='information']//strong[@class='campaign-price']");

        String nameOfProductOnMainPage = driver.findElement(nameOfProductInBlockCampaigns).getText();
        String priceRegularOnMainPage = driver.findElement(regularPriceInMainPage).getText();
        String priceCampaignsOnMainPage = driver.findElement(campaignPriceInCampaigns).getText();

        List<String> greyColorOnMainPage = checkGreyColorRegularPrice(regularPriceInMainPage);
        checkDecorationLine(regularPriceInMainPage);
        checkBoldText(campaignPriceInCampaigns);
        checkRedColorCampaignPrice(campaignPriceInCampaigns);
        checkSizesOfPrices(campaignPriceInCampaigns, regularPriceInMainPage);

        driver.get("http://localhost/litecart/en/rubber-ducks-c-1/subcategory-c-2/yellow-duck-p-1");
        String nameOfProductOnProductPage = driver.findElement(nameOfProductOnPageOfProduct).getText();
        String priceRegularOnProductPage = driver.findElement(regularPriceOnPageOfProduct).getText();
        String priceCampaignsOnProductPage = driver.findElement(campaignPriceOnPageOfProduct).getText();

        List<String> greyColorOnProductPage = checkGreyColorRegularPrice(regularPriceOnPageOfProduct);
        checkDecorationLine(regularPriceOnPageOfProduct);
        checkBoldText(campaignPriceOnPageOfProduct);
        checkRedColorCampaignPrice(campaignPriceOnPageOfProduct);
        checkSizesOfPrices(campaignPriceOnPageOfProduct, regularPriceOnPageOfProduct);

        assertingTwoObjects("Названия продукта не совпадают на главной странице и странице продукта",
                nameOfProductOnMainPage, nameOfProductOnProductPage);
        assertingTwoObjects("Регулярная цена не совпадает на главной странице и странице продукта",
                priceRegularOnMainPage, priceRegularOnProductPage);
        assertingTwoObjects("Скидочная цена не совпадает на главной странице и странице продукта",
                priceCampaignsOnMainPage, priceCampaignsOnProductPage);
        assertingTwoObjects("Цвета цен не совпадают на главной странице и странице продукта",
                greyColorOnMainPage, greyColorOnProductPage);
    }

    private List<String> checkGreyColorRegularPrice(By regularPrice) {
        Color objectColor = Color.fromString(driver.findElement(regularPrice).getCssValue("color"));
        String colorRGB = objectColor.asRgb();
        String[] massivRGB = colorRGB.substring(4, colorRGB.length() - 1).replaceAll("\\s+", "").
                split(",");
        List<String> valuesOfRGB = new ArrayList<>(Arrays.asList(massivRGB));
        Collections.sort(valuesOfRGB);
        Assert.assertTrue("Значения RGB не равны - цвет не серый", valuesOfRGB.get(0).
                equals(valuesOfRGB.get(2)));
        return valuesOfRGB;
    }

    private void checkDecorationLine(By regularPrice) {
        String nameOfTag = driver.findElement(regularPrice).getTagName();
        Assert.assertTrue("Регулярная цена не зачеркнута", nameOfTag.equals("s"));
    }

    private void checkBoldText(By campaignPrice) {
        String nameOfTag = driver.findElement(campaignPrice).getTagName();
        Assert.assertTrue("Текст скидочной цены не жирный", nameOfTag.equals("strong"));
    }

    private void checkRedColorCampaignPrice(By campaignPrice) {
        Color objectColor = Color.fromString(driver.findElement(campaignPrice).getCssValue("color"));
        String colorRGB = objectColor.asRgb();
        String[] massivRGB = colorRGB.substring(4, colorRGB.length() - 1).
                replaceAll("\\s+", "").split(",");
        List<String> valuesOfRGB = new ArrayList<>(Arrays.asList(massivRGB));
        Assert.assertTrue("Значения GB не 0 - цвет не красный", valuesOfRGB.get(1).equals(valuesOfRGB.get(2)));
    }

    private void checkSizesOfPrices(By campaignPrice, By regularPrice) {
        String sizeOfCampaignPrice = (driver.findElement(campaignPrice).getCssValue("font-size"));
        String sizeOfRegularPrice = (driver.findElement(regularPrice).getCssValue("font-size"));
        Float sizeOfCampaignPriceInt = Float.parseFloat(sizeOfCampaignPrice.
                substring(0, sizeOfCampaignPrice.length() - 2));
        Float sizeOfRegularPriceInt = Float.parseFloat(sizeOfRegularPrice.
                substring(0, sizeOfRegularPrice.length() - 2));
        Assert.assertTrue("Скидочная цена меньше размером регулярной",
                sizeOfCampaignPriceInt > sizeOfRegularPriceInt);
    }

    private void assertingTwoObjects(String errorText, Object first, Object second) {
        try {
            Assert.assertTrue(errorText, first.equals(second));
        } catch (AssertionError e) {
            e.printStackTrace();
        }
    }

    @After
    public void closeBrowser() {
        driver.quit();
        driver = null;
    }
}
