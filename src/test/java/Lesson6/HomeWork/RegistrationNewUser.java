package Lesson6.HomeWork;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.SourceType;
import org.openqa.selenium.support.ui.Select;
import sun.java2d.pipe.SpanShapeRenderer;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

/*
 * Сделайте сценарий для регистрации нового пользователя в учебном приложении litecart (не в админке, а в клиентской части магазина).
 * Сценарий должен состоять из следующих частей:
 * 1) регистрация новой учётной записи с достаточно уникальным адресом электронной почты (чтобы не конфликтовало с ранее созданными пользователями, в том числе при предыдущих запусках того же самого сценария),
 * 2) выход (logout), потому что после успешной регистрации автоматически происходит вход,
 * 3) повторный вход в только что созданную учётную запись,
 * 4) и ещё раз выход.
 * В качестве страны выбирайте United States, штат произвольный. При этом формат индекса -- пять цифр.
 */

public class RegistrationNewUser {
    private WebDriver driver;

    @Before
    public void startChrome() {
        ChromeOptions options = new ChromeOptions();
        options.setBinary("C:\\Users\\Serg\\AppData\\Local\\Google\\Chrome\\Application\\chrome.exe");
        options.addArguments("user-data-dir=C:\\Users\\Serg\\AppData\\Local\\Google\\Chrome\\User Data\\Default1");
        options.addArguments("--incognito");
        options.addArguments("--start-maximized");
        driver = new ChromeDriver(options);
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
    }

    @Test
    public void testRegistration() {
        String userMail = testRegUser();
        logOut();
        login(userMail, "12345");
        logOut();
    }


    public String testRegUser() {
        driver.get("http://localhost/litecart/en/");
        driver.findElement(By.xpath("//a[@href='http://localhost/litecart/en/create_account']")).click();
        SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmmssssssss");
        String timeNow = null;
        String firstname = "ermak";
        String lastname = "ermaklast";
        String address1 = "moscow";
        String postcode = "12345";
        String city = "moscow";
        String email = "ermak@1cwcewre.ru";
        String phone = "+79500001160";
        String password = "12345";
        String confirmedPassword = "12345";

        driver.findElement(By.xpath("//select[@name='country_code']/option[@value='US']")).click();
        driver.findElement(By.xpath("//input[@name='firstname']")).sendKeys(firstname);
        driver.findElement(By.xpath("//input[@name='lastname']")).sendKeys(lastname);
        driver.findElement(By.xpath("//input[@name='address1']")).sendKeys(address1);
        driver.findElement(By.xpath("//input[@name='postcode']")).sendKeys(postcode);
        driver.findElement(By.xpath("//input[@name='city']")).sendKeys(city);
        driver.findElement(By.xpath("//input[@name='phone']")).sendKeys(phone);
        fillPasswordsAndMail(email, password, confirmedPassword);

        if (driver.findElements(By.xpath("(//a[@href='http://localhost/litecart/en/order_history'])[1]")).size() < 1) {
            while (driver.findElement(By.xpath("//div[contains(@class,'notice') and contains(@class,'errors')]")).isDisplayed()) {
                timeNow = dateFormat.format(Calendar.getInstance().getTime());
                email = "ermak@" + timeNow + ".ru";
                fillPasswordsAndMail(email, password, confirmedPassword);
                if (driver.findElements(By.xpath("(//a[@href='http://localhost/litecart/en/order_history'])[1]")).size() > 0) {
                    break;
                }
            }
        }
        System.out.println("Регистрация успешна");
        return email;
    }

    public void fillPasswordsAndMail(String email, String password, String confirmedPassword) {
        driver.findElement(By.xpath("//input[@name='email']")).clear();
        driver.findElement(By.xpath("//input[@name='email']")).sendKeys(email);
        driver.findElement(By.xpath("//input[@name='password']")).sendKeys(password);
        driver.findElement(By.xpath("//input[@name='confirmed_password']")).sendKeys(confirmedPassword);
        driver.findElement(By.xpath("//button[@name='create_account']")).click();
    }

    public void login(String mail, String pass) {
        driver.get("http://localhost/litecart/en/logout");
        driver.findElement(By.xpath("//input[@name='email']")).clear();
        driver.findElement(By.xpath("//input[@name='email']")).sendKeys(mail);
        driver.findElement(By.xpath("//input[@name='password']")).sendKeys(pass);
        driver.findElement(By.xpath("//button[@type='submit' and @name='login']")).click();
        if (driver.findElements(By.xpath("(//a[@href='http://localhost/litecart/en/order_history'])[1]")).size() > 0) {
            System.out.println("Вы залогинены");
        }
    }

    public void logOut() {
        driver.findElement(By.xpath("(//a[@href='http://localhost/litecart/en/logout'])[1]")).click();
        if (driver.findElements(By.xpath("(//a[@href='http://localhost/litecart/en/order_history'])[1]")).size() < 1) {
            System.out.println("Разлогин");
        }
    }

    public void closeBrowser(){
        driver.quit();
        driver = null;
    }
}
