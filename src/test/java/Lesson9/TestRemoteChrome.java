package Lesson9;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Platform;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.MalformedURLException;
import java.net.URL;

public class TestRemoteChrome {

    private RemoteWebDriver driver;
    private WebDriverWait wait;

    @Before
    public void startChrome() throws MalformedURLException {

        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setBrowserName("chrome");
        //capabilities.setPlatform(Platform.WINDOWS);
        //driver = new RemoteWebDriver(new URL("http://192.168.1.232:4444/wd/hub"), capabilities);
        capabilities.setPlatform(Platform.WINDOWS);
        capabilities.setVersion("65");
        driver = new RemoteWebDriver(new URL("https://sergkor1:cSufsKLYnaYeLCDsBTB1@hub-cloud.browserstack.com/wd/hub"), capabilities);
    }

    @Test
    public void test() {
        driver.get("http://yandex.ru/");
    }

}
