package Lesson7.Homework;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import javax.xml.xpath.XPath;
import java.util.List;
import java.util.concurrent.TimeUnit;

/* Задание 13. Сделайте сценарий работы с корзиной
 * Сделайте сценарий для добавления товаров в корзину и удаления товаров из корзины.
 * 1) открыть главную страницу
 * 2) открыть первый товар из списка
 * 2) добавить его в корзину (при этом может случайно добавиться товар, который там уже есть, ничего страшного)
 * 3) подождать, пока счётчик товаров в корзине обновится
 * 4) вернуться на главную страницу, повторить предыдущие шаги ещё два раза, чтобы в общей сложности в корзине было 3 единицы товара
 * 5) открыть корзину (в правом верхнем углу кликнуть по ссылке Checkout)
 * 6) удалить все товары из корзины один за другим, после каждого удаления подождать, пока внизу обновится таблица
 */

public class AddToBasket {
    WebDriver driver;
    WebDriverWait wait;

    @Before
    public void startChrome() {
        ChromeOptions options = new ChromeOptions();
        //options.setBinary("C:\\Users\\Serg\\AppData\\Local\\Google\\Chrome\\Application\\chrome.exe");
        //options.addArguments("user-data-dir=C:\\Users\\Serg\\AppData\\Local\\Google\\Chrome\\User Data\\Default1");
        options.setBinary("C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe");
        options.addArguments("user-data-dir=C:\\Users\\Tester\\AppData\\Local\\Google\\Chrome\\User Data\\Default1");
        options.addArguments("--incognito");
        options.addArguments("--start-maximized");
        driver = new ChromeDriver(options);
        wait = new WebDriverWait(driver, 10);
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
    }

    @Test
    public void testAddProduct() {

        for (int i = 1; i < 4; i++) {
            String expectedNumberOfGoodsInCounter = String.valueOf(i);
            driver.get("http://localhost/litecart/en/");
            findElement("(//li[contains(@class,'product')])[1]").click();
            String numberOfProductsInCart = findElement("//span[@class='quantity']").getText();
            if (findElements("//select[@name='options[Size]']").size() > 0) {
                findElement("//select[@name='options[Size]']/option[@value='Small']").click();
            }
            findElement("//button[@name='add_cart_product']").click();
            WebElement element = findElement("//span[@class='quantity']");
            wait.until(ExpectedConditions.textToBePresentInElement(element, expectedNumberOfGoodsInCounter));
            driver.navigate().back();
        }
        findElement("//a[@href='http://localhost/litecart/en/checkout'][@class='link']").click();
        List numberOfDifferentsGoods = countGoodsInCart();
        while (numberOfDifferentsGoods.size() > 0) {
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//button[@name='remove_cart_item'])[1]")));
            String nameOfProduct = findElement("(//a[starts-with(@href,'http://localhost/litecart/en')]" +
                    "//strong[contains(text(),'')])[1]").getText();
            findElement("(//button[@name='remove_cart_item'])[1]").click();
            wait.until(ExpectedConditions.stalenessOf(findElement("//td[@class='item'][contains(text(),'" +
                    nameOfProduct + "')]")));
            System.out.println("Удалили товар - " + nameOfProduct);
            numberOfDifferentsGoods = countGoodsInCart();
        }
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//em[contains(text()," +
                "'There are no items in your cart.')]")));
        System.out.println("В корзине ничего нет");
    }


    public WebElement findElement(String path) {
        return driver.findElement(By.xpath(path));
    }

    public List<WebElement> findElements(String path) {
        return driver.findElements(By.xpath(path));
    }

    public List countGoodsInCart() {
        List<WebElement> numberOfDifferentsGoods = findElements("//a[starts-with(@href," +
                "'http://localhost/litecart/en')][contains(@class,'image-wrapper')]");
        return numberOfDifferentsGoods;
    }
}



