package Lesson5.Homework;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

/*
 * Задание 9. Проверить сортировку стран и геозон в админке
 * 2) на странице http://localhost/litecart/admin/?app=geo_zones&doc=geo_zones
 * зайти в каждую из стран и проверить, что зоны расположены в алфавитном порядке
 */

public class CheckSortingGeoZones {

    private WebDriver driver;

    @Before
    public void startChrome() {

        ChromeOptions options = new ChromeOptions();
        options.setBinary("C:\\Users\\Serg\\AppData\\Local\\Google\\Chrome\\Application\\chrome.exe");
        options.addArguments("user-data-dir=C:\\Users\\Serg\\AppData\\Local\\Google\\Chrome\\User Data\\Default1");
        options.addArguments("--incognito");
        options.addArguments("--start-maximized");
        driver = new ChromeDriver(options);
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
    }

    @Test
    public void checkSort() {
        driver.get("http://localhost/litecart/admin");
        driver.findElement(By.name("username")).sendKeys("admin");
        driver.findElement(By.name("password")).sendKeys("admin");
        driver.findElement(By.name("login")).click();
        driver.get("http://localhost/litecart/admin/?app=geo_zones&doc=geo_zones");

        List<WebElement> elementsRowsOnPage = driver.findElements(By.xpath("//tr[@class='row']"));
        List<Integer> idOfCountries = new ArrayList<>();
        By idOfCountry = By.xpath("./td[2]");

        for (WebElement elementRow : elementsRowsOnPage) {
            idOfCountries.add(Integer.parseInt(elementRow.findElement(idOfCountry).getText()));
        }

        for (Integer id : idOfCountries) {
            ArrayList<String> namesOfZones = new ArrayList<String>();
            ArrayList<String> namesOfZonesSorted = new ArrayList<String>();
            By allSelectedZones = By.xpath("(//table[@class='dataTable' and @id='table-zones']//td[3]//option[@selected='selected'])");
            driver.get("http://localhost/litecart/admin/?app=geo_zones&doc=edit_geo_zone&page=1&geo_zone_id=" + id);
            List<WebElement> elementsZones = driver.findElements(allSelectedZones);

            for (int i = 1; i <= elementsZones.size(); i++) {
                String selectedZoneForCurrentRow = driver.findElement(By.xpath("(//table[@class='dataTable' and @id='table-zones']//td[3]//option[@selected='selected'])[" + i + "]")).getText();
                namesOfZones.add(selectedZoneForCurrentRow);
                namesOfZonesSorted.add(selectedZoneForCurrentRow);
            }

            Collections.sort(namesOfZonesSorted);
            try {
                Assert.assertEquals("Сортировка зон не по алфавиту", namesOfZonesSorted, namesOfZones);
            } catch (AssertionError e) {
                e.printStackTrace();
            }
        }
    }

    @After
    public void closeChrome() {
        driver.quit();
        driver = null;
    }
}