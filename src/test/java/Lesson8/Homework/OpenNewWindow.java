package Lesson8.Homework;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import static org.openqa.selenium.support.ui.ExpectedConditions.titleIs;

/*Задание 14. Проверьте, что ссылки открываются в новом окне
 * Сделайте сценарий, который проверяет, что ссылки на странице редактирования страны открываются в новом окне.
 * Сценарий должен состоять из следующих частей:
 * 1) зайти в админку
 * 2) открыть пункт меню Countries (или страницу http://localhost/litecart/admin/?app=countries&doc=countries)
 * 3) открыть на редактирование какую-нибудь страну или начать создание новой
 * 4) возле некоторых полей есть ссылки с иконкой в виде квадратика со стрелкой -- они ведут на внешние страницы и
 * открываются в новом окне, именно это и нужно проверить.
 * Конечно, можно просто убедиться в том, что у ссылки есть атрибут target="_blank". Но в этом упражнении требуется
 * именно кликнуть по ссылке, чтобы она открылась в новом окне, потом переключиться в новое окно, закрыть его, вернуться
 * обратно, и повторить эти действия для всех таких ссылок.
 * Не забудьте, что новое окно открывается не мгновенно, поэтому требуется ожидание открытия окна.
 */

public class OpenNewWindow {

    WebDriver driver;
    WebDriverWait wait;

    @Before
    public void startChrome() {
        ChromeOptions options = new ChromeOptions();
        options.setBinary("C:\\Users\\Serg\\AppData\\Local\\Google\\Chrome\\Application\\chrome.exe");
        options.addArguments("user-data-dir=C:\\Users\\Serg\\AppData\\Local\\Google\\Chrome\\User Data\\Default1");
        //options.setBinary("C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe");
        //options.addArguments("user-data-dir=C:\\Users\\Tester\\AppData\\Local\\Google\\Chrome\\User Data\\Default1");
        options.addArguments("--incognito");
        options.addArguments("--start-maximized");
        driver = new ChromeDriver(options);
        wait = new WebDriverWait(driver, 10);
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
    }

    @Test
    public void testNewWindow() {
        driver.get("http://localhost/litecart/admin/logout.php");
        driver.findElement(By.name("username")).sendKeys("admin");
        driver.findElement(By.name("password")).sendKeys("admin");
        driver.findElement(By.name("remember_me")).click();
        driver.findElement(By.name("login")).click();
        wait.until(titleIs("My Store"));
        driver.get("http://localhost/litecart/admin/?app=countries&doc=countries");
        driver.findElement(By.xpath("//a[@class='button'][starts-with(@href," +
                "'http://localhost/litecart/admin/?app=countrie')]")).click();

        List<WebElement> allLinks = driver.findElements(By.xpath("//i[contains(@class,'fa-external-link')]"));

        for (WebElement link : allLinks) {
            String primaryWindow = driver.getWindowHandle();
            Set<String> allWindows = driver.getWindowHandles();
            link.click();
            wait.until(anyWindowOtherThen(allWindows));

            for (String winHandle : driver.getWindowHandles()) {
                driver.switchTo().window(winHandle);
                if (!driver.getWindowHandle().equals(primaryWindow)) {
                    try {
                        Assert.assertFalse("Вкладка не открыта или страница не загружена",
                                driver.getTitle() == null);
                    } catch (AssertionError e) {
                        e.printStackTrace();
                    }
                    System.out.println("Заголовок - " + driver.getTitle());
                }
            }
            driver.close();
            driver.switchTo().window(primaryWindow);
        }
    }

    @After
    public void closeBrowser() {
        driver.quit();
        driver = null;
    }

    public ExpectedCondition<String> anyWindowOtherThen(Set<String> oldWindows) {
        return new ExpectedCondition<String>() {
            public String apply(WebDriver driver) {
                Set<String> handles = driver.getWindowHandles();
                handles.removeAll(oldWindows);
                return handles.size() > 0 ? handles.iterator().next() : null;
            }
        };
    }
}



