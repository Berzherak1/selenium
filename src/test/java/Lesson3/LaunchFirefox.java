package Lesson3;

import com.google.common.collect.ImmutableMap;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LaunchFirefox {

    private WebDriver driver;
    private WebDriverWait wait;

    @Before
    public void startFirefox() {
        FirefoxOptions options = new FirefoxOptions();
        System.setProperty("webdriver.gecko.driver", "C:\\Webserver\\geckodriver.exe");
        options.addArguments("--incognito");
        options.addArguments("---start-maximized");
//        FirefoxProfile firefoxProfile = new FirefoxProfile();
//        firefoxProfile.setPreference("browser.private.browsing.autostart", true);
//        options.setProfile(firefoxProfile);
        //options.addArguments("-console");
        //options.addPreference("browser.cache.disk.enable", false);

        driver = new FirefoxDriver(options);
        driver.manage().window().maximize();
    }

    ;

    @Test
    public void test() {
        driver.get("http://yandex.ru/");

    }

    @After

    public void closeBrowser(){

        driver.quit();
        driver=null;
    }

}

