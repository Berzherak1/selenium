package Lesson10.Homework10;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import static org.openqa.selenium.support.ui.ExpectedConditions.titleIs;

/*
 * Задание 17. Проверьте отсутствие сообщений в логе браузера
 * Сделайте сценарий, который проверяет, не появляются ли в логе браузера сообщения при открытии страниц в учебном
 * приложении, а именно -- страниц товаров в каталоге в административной панели.
 * Сценарий должен состоять из следующих частей:
 * 1) зайти в админку
 * 2) открыть каталог, категорию, которая содержит товары
 * (страница http://localhost/litecart/admin/?app=catalog&doc=catalog&category_id=1)
 * 3) последовательно открывать страницы товаров и проверять, не появляются ли в логе браузера сообщения (любого уровня)
 */

public class CheckLog {
    public WebDriver driver;
    public WebDriverWait wait;

    @Before
    public void startChrome() {
        ChromeOptions options = new ChromeOptions();
//        options.setBinary("C:\\Users\\Serg\\AppData\\Local\\Google\\Chrome\\Application\\chrome.exe");
//        options.addArguments("user-data-dir=C:\\Users\\Serg\\AppData\\Local\\Google\\Chrome\\User Data\\Default1");
        options.setBinary("C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe");
        options.addArguments("user-data-dir=C:\\Users\\Tester\\AppData\\Local\\Google\\Chrome\\User Data\\Default1");
        options.addArguments("--incognito");
        options.addArguments("--start-maximized");
        LoggingPreferences logPrefs = new LoggingPreferences();
        logPrefs.enable(LogType.PERFORMANCE, Level.ALL);
        options.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
        driver = new ChromeDriver(options);
        wait = new WebDriverWait(driver, 10);
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
    }

    @Test
    public void checkLog() {
        driver.get("http://localhost/litecart/admin/logout.php");
        driver.findElement(By.name("username")).sendKeys("admin");
        driver.findElement(By.name("password")).sendKeys("admin");
        driver.findElement(By.name("remember_me")).click();
        driver.findElement(By.name("login")).click();
        wait.until(titleIs("My Store"));
        driver.get("http://localhost/litecart/admin/?app=catalog&doc=catalog&category_id=1");
        List<WebElement> goods = driver.findElements(By.xpath("//tr[@class='row']/td/a[contains(text(),'Duck')]"));
        for (int i = 1; i <= goods.size(); i++) {
            driver.findElement(By.xpath("(//tr[@class='row']/td/a[contains(text(),'Duck')])[" + i + "]")).click();
            for (LogEntry l : driver.manage().logs().get("browser").getAll()) {
                System.out.println(l);
            }
            driver.navigate().back();
        }
    }

    @After
    public void closeBrowser() {
        driver.quit();
        driver = null;
    }
}
