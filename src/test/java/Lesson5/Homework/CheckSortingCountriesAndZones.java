package Lesson5.Homework;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.SourceType;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

/*
* Задание 9. Проверить сортировку стран и геозон в админке
* Сделайте сценарии, которые проверяют сортировку стран и геозон (штатов) в учебном приложении litecart.
* 1) на странице http://localhost/litecart/admin/?app=countries&doc=countries
* а) проверить, что страны расположены в алфавитном порядке
* б) для тех стран, у которых количество зон отлично от нуля -- открыть страницу этой страны и там проверить,
* что зоны расположены в алфавитном порядке
*/

public class CheckSortingCountriesAndZones {

    private WebDriver driver;

    @Before
    public void startChrome() {

        ChromeOptions options = new ChromeOptions();
        options.setBinary("C:\\Users\\Serg\\AppData\\Local\\Google\\Chrome\\Application\\chrome.exe");
        options.addArguments("user-data-dir=C:\\Users\\Serg\\AppData\\Local\\Google\\Chrome\\User Data\\Default1");
        options.addArguments("--incognito");
        options.addArguments("--start-maximized");
        driver = new ChromeDriver(options);
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
    }

    @Test
    public void checkSort() {
        driver.get("http://localhost/litecart/admin");
        driver.findElement(By.name("username")).sendKeys("admin");
        driver.findElement(By.name("password")).sendKeys("admin");
        driver.findElement(By.name("login")).click();
        driver.get("http://localhost/litecart/admin/?app=countries&doc=countries");

        ArrayList<String> namesOfCountries = new ArrayList<String>();
        ArrayList<String> namesOfCountriesSorted = new ArrayList<String>();
        ArrayList<String> namesOfCodesOfCountries = new ArrayList<String>();
        List<WebElement> elementsRowsOnPage = driver.findElements(By.xpath("//tr[@class='row']"));

        By nameOfCountry = By.xpath(".//a[starts-with(@href,'http://localhost/litecart/admin/?app=countries&doc=edit_country&country_code')][@title=not('Edit')]");
        By numberOfZones = By.xpath("./td[6]");
        By codeOfCountry = By.xpath("./td[4]");

        for (WebElement elementRow : elementsRowsOnPage) {
            namesOfCountries.add(elementRow.findElement(nameOfCountry).getText());
            namesOfCountriesSorted.add(elementRow.findElement(nameOfCountry).getText());

            int quantity = Integer.parseInt(elementRow.findElement(numberOfZones).getText());
            if (quantity > 0) {
                namesOfCodesOfCountries.add(elementRow.findElement(codeOfCountry).getText());
            }

            Collections.sort(namesOfCountriesSorted);
            assertingLists("Сортировка стран не по алфавиту", namesOfCountriesSorted, namesOfCountries);
        }

        for (String code : namesOfCodesOfCountries) {
            driver.get("http://localhost/litecart/admin/?app=countries&doc=edit_country&country_code=" + code);
            CheckSortZonesInsidePageCountry();
        }
    }

    public void CheckSortZonesInsidePageCountry() {

        List<WebElement> zonesOfCountryInTable = driver.findElements(By.xpath("//table[@id='table-zones']//tr/td[3]"));
        ArrayList<String> namesOfZones = new ArrayList<String>();
        ArrayList<String> namesOfZonesSorted = new ArrayList<String>();

        for (int i = 1; i < zonesOfCountryInTable.size(); i++) {
            String nameOfZoneInTable = driver.findElement(By.xpath("(//table[@id='table-zones']//tr/td[3])[" + i + "]")).getText();
            namesOfZones.add(nameOfZoneInTable);
            namesOfZonesSorted.add(nameOfZoneInTable);
            Collections.sort(namesOfZonesSorted);
            assertingLists("Сортировка зон не по алфавиту", namesOfZonesSorted, namesOfZones);
        }
    }

    public void assertingLists(String message, List expectedSortList, List actualList) {
        try {
            Assert.assertEquals(message, expectedSortList, actualList);
        } catch (AssertionError e) {
            e.printStackTrace();
        }
    }

    @After
    public void closeChrome() {
        driver.quit();
        driver = null;
    }
}