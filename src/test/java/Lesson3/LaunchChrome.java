package Lesson3;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.MalformedURLException;
import java.net.URL;

public class LaunchChrome {

    private RemoteWebDriver driver;
    private WebDriverWait wait;

    @Before
    public void startChrome() throws MalformedURLException {

        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setBrowserName("chrome");
        capabilities.setPlatform(Platform.WINDOWS);
        ChromeOptions options = new ChromeOptions();
//        options.setBinary("C:\\Users\\Serg\\AppData\\Local\\Google\\Chrome\\Application\\chrome.exe");
//        options.addArguments("user-data-dir=C:\\Users\\Serg\\AppData\\Local\\Google\\Chrome\\User Data\\Default1");
        options.setBinary("C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe");
        options.addArguments("user-data-dir=C:\\Users\\Tester\\AppData\\Local\\Google\\Chrome\\User Data\\Default1");
        options.addArguments("--incognito");
        options.addArguments("--start-maximized");
        //driver = new ChromeDriver(options);
        //driver = new RemoteWebDriver(new URL("http://192.168.1.232:4444/wd/hub"),capabilities);
        driver = new RemoteWebDriver(new URL("http://10.0.2.15:5555/wd/hub"),capabilities);
//        options.addArguments("--disable-extensions");
//        DesiredCapabilities caps = new DesiredCapabilities();
//        caps.setCapability(ChromeOptions.CAPABILITY,options);
    }

    @Test
    public void test() {
        driver.get("http://yandex.ru/");
    }

    //@After
    public void closeChrome() {
        driver.quit();
        driver = null;
    }
}