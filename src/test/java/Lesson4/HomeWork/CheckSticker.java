package Lesson4.HomeWork;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Задание 8. Сделайте сценарий, проверяющий наличие стикеров у товаров
 * Сделайте сценарий, проверяющий наличие стикеров у всех товаров в учебном приложении litecart на главной странице.
 * Стикеры -- это полоски в левом верхнем углу изображения товара,
 * на которых написано New или Sale или что-нибудь другое.
 * Сценарий должен проверять, что у каждого товара имеется ровно один стикер.
 */
public class CheckSticker {

    private WebDriver driver;

    @Before
    public void startChrome() {

        ChromeOptions options = new ChromeOptions();
        options.setBinary("C:\\Users\\Serg\\AppData\\Local\\Google\\Chrome\\Application\\chrome.exe");
        options.addArguments("user-data-dir=C:\\Users\\Serg\\AppData\\Local\\Google\\Chrome\\User Data\\Default1");
        options.addArguments("--incognito");
        options.addArguments("--start-maximized");
        driver = new ChromeDriver(options);
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
    }

    @Test
    public void checkSticker() {

        driver.get("http://localhost/litecart/");
        List<WebElement> totalElements = new ArrayList<>();
        totalElements = driver.findElements(By.xpath("//li[contains(@class,'product')]"));

        for (WebElement element : totalElements) {

            String nameOfProduct = element.findElement(By.xpath(".//div[@class='name']")).getText();
            Integer numberOfStickers = element.findElements(By.xpath(".//div[starts-with(@class, 'sticker')]")).size();

            try {
                if (numberOfStickers > 1) {
                    Assert.fail("У товара " + nameOfProduct + " больше одного стикера");
                }
            } catch (AssertionError e) {
                e.printStackTrace();
            }
        }
    }

    @After
    public void closeChrome() {
        driver.quit();
        driver = null;
    }
}
