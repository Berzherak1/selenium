package Lesson3;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LaunchIE {

    private WebDriver driver;
    private WebDriverWait wait;

    @Before
    public void startIE(){
         InternetExplorerOptions options = new InternetExplorerOptions();
        options.setCapability(InternetExplorerDriver.REQUIRE_WINDOW_FOCUS, true);


        driver = new InternetExplorerDriver(options);
    };

    @Test
    public void test(){
        driver.get("http://yandex.ru/");

    };
}
